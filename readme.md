
# MinecraftAssets

## About
This is a very simple Java application to download the assets for any given version of Minecraft. I created it in about 10 minutes for use on a personal project, but figured it might be useful to others, so I uploaded it here. I'm not going to spend time maintaining it, but it'll probably work for the foreseeable future until Mojang change something on their end. Don't expect clean or efficient code, it does the job, and that's good enough for how often I expect to use this.

I should point out it only downloads assets that aren't bundled into the minecraft.jar at the moment. This may change in the future, but for now this is just downloading all the awkward files, not the ones you can easily extract.

## Dependencies
This project uses [json-simple](https://code.google.com/archive/p/json-simple/) and [jopt-simple](http://jopt-simple.github.io/jopt-simple/) to work, both of which are packaged into the jar in the downloads section.

## Usage
Download the jar file from the tab on the left (if BitBucket has the same layout as it did when I wrote this), or compile it yourself from the source code. You'll need to package the two dependencies mentioned above into the jar if compiling yourself.

Run the jar file through the command line, in the directory you intend to download assets to.
`java -jar minecraft-assets-1.0.0.jar [options]`
### Avaliable Options

 - `--help` Displays a help menu, showing you these options
 - `--showVersions` Shows a list of all available versions
 - `--version [id]` Downloads assets for the given version. If omitted, it'll default to the latest full release of the game

## Contributing and Licence
As I said in the intro, this was mainly a tool for personal use. Feel free to fork it, modify it, do whatever you like. Just don't try and claim it as your own. Beyond that, do whatever you want, I don't care.
 
:)