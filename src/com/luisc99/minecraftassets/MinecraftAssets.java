package com.luisc99.minecraftassets;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;

public class MinecraftAssets
{
	private static HashMap<String, String> versionUrls = new HashMap<String, String>();
	private static File baseDir;
	
	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws IOException, ParseException
	{
		System.out.println("");
		System.out.println("MinecraftAssets 1.0.0");
		System.out.println("  Copyright luisc99, 2019");
		
		OptionParser optionparser = new OptionParser();
		optionparser.allowsUnrecognizedOptions();
			OptionSpec<Void> showHelp = optionparser.accepts("help", "Shows this help menu").forHelp();
			OptionSpec<Void> showVersions = optionparser.accepts("showVersions", "Displays the list of avaliable versions").forHelp();
			OptionSpec<String> _version = optionparser.accepts("version", "The version to download. Defaults to latest").withOptionalArg().defaultsTo("latest");
		OptionSet options = optionparser.parse(args);
			String version = _version.value(options);
		
		JSONParser parser = new JSONParser();
		
		
		
		
		
		if(options.has(showHelp))
		{
			System.out.println("MinecraftAssets - Copyright luisc99 2019");
			System.out.println("This is a simple program to download the assets of any minecraft version. I made it in about 10 minutes, and offer it with no guarentee it'll work, or any promise I'll fix it if it's broken");
			System.out.println("- Luis");
			System.out.println("  ");
			System.out.println("  --help                Displays this help menu");
			System.out.println("  --showVersions        Shows a list of all avaliable versions");
			System.out.println("  --version [version]   Downloads the assets for a specific version. Defaults to latest release");
			System.out.println("");
			System.exit(0);
		}
		
		
		
		
		
		/* Gets a list of all the versions */
		Reader reader = new BufferedReader(new InputStreamReader(new URL("https://launchermeta.mojang.com/mc/game/version_manifest.json").openStream()));
	
    	// Gets the JSON object from the URL above
        JSONObject jsonObject = (JSONObject) parser.parse(reader);

        
        
        
        
        // Determines the latest version, and displays the requested version
        if(!options.has(showVersions))
        {
            JSONObject latestArray = (JSONObject) jsonObject.get("latest");
            String latest = (String) latestArray.get("release");
        	System.out.println("Latest version from Mojang is " + latest);
        	if(version == "latest") { System.out.println("Requested latest, using version " + latest); version = latest; }
        	else { System.out.println("Requested " + version); }
        }

        
        
        
        
        // Loop through the array of all versions
        if(options.has(showVersions)) { System.out.println("\nAvaliable versions:"); }
        JSONArray versions = (JSONArray) jsonObject.get("versions");
        Iterator<JSONObject> iterator = versions.iterator();
        while (iterator.hasNext())
        {
            JSONObject ver = iterator.next();
            String id = (String) ver.get("id");
            String url = (String) ver.get("url");
            versionUrls.put(id, url);
            	if(options.has(showVersions)) { System.out.println("  > " + id); }
        }
        if(options.has(showVersions)) { System.out.println(""); System.exit(0); }
        
        
        
        
        
        // Gets version info
        if(versionUrls.containsKey(version))
        {
        	// Creates the directory, deleting it if it already exists
        	baseDir = new File("assets/" + version);
        	if(baseDir.exists()) { System.out.println("Asset directory already exists, deleting"); baseDir.delete(); }
        	baseDir.mkdirs();
        	
        	// Gets the version asset url
        	String versionUrl = versionUrls.get(version);
        	Reader versionReader = new BufferedReader(new InputStreamReader(new URL(versionUrl).openStream()));
        	JSONObject versionJson = (JSONObject) parser.parse(versionReader);
        	JSONObject assetIndex = (JSONObject) versionJson.get("assetIndex");
        	String assetUrl = (String) assetIndex.get("url");
        	
        	// Gets the objects
        	Reader assetReader = new BufferedReader(new InputStreamReader(new URL(assetUrl).openStream()));
        	JSONObject assetsJson = (JSONObject) parser.parse(assetReader);
        	JSONObject objects = (JSONObject) assetsJson.get("objects");
        	
        	// Iterates through the objects
        	int thisDownload = 1;
        	int totalDownloads = objects.size();
        	Set<String> keySet = objects.keySet();
        	Iterator<String> keys = keySet.iterator();
        	while(keys.hasNext())
        	{
        		String key = keys.next();
        		JSONObject objectInfo = (JSONObject) objects.get(key);
        		String hash = (String) objectInfo.get("hash");
        		
        		downloadFile(hash, key, thisDownload++, totalDownloads);
        	}
        }
        else { System.out.println("Could not find requested version"); }
        
        
        
        System.out.println("Finished downloading assets for Minecraft " + version);
        System.out.println("Thanks for using this program <3");
        System.out.println("");
        System.exit(0);
	}
	
	
	
	
	
	private static void downloadFile(String hash, String name, int thisDownload, int totalDownloads)
	{
		URL url;
        URLConnection con;
        DataInputStream dis; 
        FileOutputStream fos; 
        byte[] fileData;  
        try
        {
        	String hashFirstTwo = hash.substring(0, 2);
        	
        	DecimalFormat df = new DecimalFormat("0.00");
        	double progress = (double) thisDownload / totalDownloads;
        	System.out.println("  [" + df.format(progress * 100) + "% - " + thisDownload + "/" + totalDownloads + "] " + name);
        	
            url = new URL("http://resources.download.minecraft.net/" + hashFirstTwo + "/" + hash);
            con = url.openConnection();
            dis = new DataInputStream(con.getInputStream());
            fileData = new byte[con.getContentLength()];
            for (int q = 0; q < fileData.length; q++) {  fileData[q] = dis.readByte(); }
            dis.close();
            
            File output = new File(baseDir, name);
            	output.getParentFile().mkdirs();
            	output.createNewFile();
            fos = new FileOutputStream(output);
            fos.write(fileData);
            fos.close();
        }
        catch(Exception m) { System.out.println(m); }
	}
}
